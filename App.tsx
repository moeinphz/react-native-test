import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { useState } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import { Appbar, Avatar, Button, DefaultTheme, List, Provider as PaperProvider, Searchbar } from 'react-native-paper';
import { black } from 'react-native-paper/lib/typescript/styles/colors';


export default function App() {

  const [users, setUsers] = useState([
    {id: 1, name: 'John', userName: 'john_me'},
    {id: 2, name: 'Steve', userName: 'steve'},
    {id: 3, name: 'Jack', userName: 'jack'},
    {id: 4, name: 'Don', userName: 'don_me'},
    {id: 5, name: 'Ben', userName: 'ben'},
    {id: 6, name: 'Mike', userName: 'mike'},
    {id: 7, name: 'Harvey', userName: 'harvey_me'},
    {id: 8, name: 'Daniel', userName: 'dani'},
    {id: 9, name: 'David', userName: 'david'},
  ])

  const state = {
    loading: false,
    data: [],
    page: 1,
    seed: 1,
    error: null,
    refreshing: false,
    onSearch: false,
  };

  return (
    <PaperProvider theme={theme}>
      
        {
          state.onSearch ? (
            <Appbar.Header focusable dark={true}>
              <Searchbar
                style={styles.searchBar}
                placeholder="Search"/>
            </Appbar.Header>
          ) : (
            <Appbar.Header focusable dark={true}>
              <Appbar.Content focusable title="Users" />
              <Appbar.Action icon="magnify" onPress={() => state.onSearch = true} /> 
            </Appbar.Header>
          )
        }
        
        
    
      <FlatList
          data={users}
          renderItem={({ item }) => (
            // <View style={styles.listItem}>
            //   <Text>{item.name}</Text>
            //   <Text>{item.userName}</Text>
            // </View>
            <List.Item
              style={styles.listItem}
              title={item.name}
              description={item.userName}
              left={props => 
                <Avatar.Image style={styles.avatar} size={64} source={{uri: 'https://i.stack.imgur.com/uoVWQ.png'}} />
              }
            />

          )}
          keyExtractor={item => item.id.toString()}
          // selected={
          //   // Can be any prop that doesn't collide with existing props
          //   this.state.selected // A change to selected should re-render FlatList
          // }
/>
    </PaperProvider>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchBar: {
    borderRadius: 6,
    backgroundColor: '#787878',
    shadowOpacity: 0,
    borderBottomColor: 'black',
    borderBottomWidth: 2,
    tintColor: 'whitesmoke',
    overlayColor: 'black',
    textDecorationColor: 'black',
    color: 'black'
  },
  listItem: {
    padding: 20,
    borderBottomColor: '#efefef',
    borderBottomWidth: 1
  },
  avatar: {
    shadowColor: '#000',
    shadowOffset: { width: 2, height: 4 },
    shadowOpacity: 0.15,
    shadowRadius: 7,
    elevation: 1,
  }
});

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    backgroundColor: 'white',
    primary: 'black',
    accent: 'red',
  }
};
